import { resource } from "../../shared/utils/resource";

interface IRegistrationDTO {
  password: string;
  email: string;
  agreeWithTerms: boolean;
}

const API_URL = import.meta.env.VITE_API_URL;

export const register = resource<IRegistrationDTO>({
  url: `${API_URL}/auth/registration`,
  method: "POST",
  withCredentials: true
});
