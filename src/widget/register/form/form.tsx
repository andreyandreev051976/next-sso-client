import { FormProvider } from "react-hook-form";
import { useRegistration } from "../registration.logic";
import { IForm } from "./form.type";
import { Field } from "./components/field";
import { Box } from "@chakra-ui/react";

export const Form = ({ children, ...props }: IForm) => {
  const { form, onSubmit } = useRegistration();

  return (
    <FormProvider {...form}>
      <Box as="form" {...props} onSubmit={form.handleSubmit(onSubmit)}>
        {children}
      </Box>
    </FormProvider>
  );
};

Form.Field = Field;
