import { useTranslation } from "react-i18next";
import { Frame } from "../../shared/components/frame";
import { registerCore } from "./registration.logic";
import { Form } from "./form";
import { Username } from "./fields/username";
import { Password } from "./fields/password";
import { Submit } from "./fields/submit";
import { VStack } from "@chakra-ui/react";
import { NavigateToLogin } from "../login/navigate";
import { AgreeWithTerms } from "./fields/agreeWithTerms";

export const RegisterForm = () => {
  const { t } = useTranslation();
  return (
    <registerCore.Provider>
      <Frame w="100%" maxW="440px" h="600px" display="flex" flexDir="column">
        <Frame.Title>{t("register:Registration")}</Frame.Title>
        <Form display="flex" flexDir="column" h="full">
          <Form.Field>
            <Username />
          </Form.Field>
          <Form.Field>
            <Password />
          </Form.Field>
          <Form.Field>
            <AgreeWithTerms />
          </Form.Field>
          <VStack mt="auto">
            <NavigateToLogin />
            <Submit w="full" />
          </VStack>
        </Form>
      </Frame>
    </registerCore.Provider>
  );
};
