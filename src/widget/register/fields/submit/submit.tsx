import { Button } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";
import { ISubmit } from "./submit.type";
import { registerCore } from "../../registration.logic";

export const Submit = ({ type = "submit", children, ...props }: ISubmit) => {
  const { t } = useTranslation();
  const { state } = registerCore.useResource();

  return (
    <Button {...props} isLoading={state.isLoading} type={type}>
      {children || t("shared:Continue")}
    </Button>
  );
};
