import { ButtonProps } from "@chakra-ui/react";

export interface ISubmit extends ButtonProps {}
