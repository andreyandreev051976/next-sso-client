import { Checkbox, FormControl, FormErrorMessage } from "@chakra-ui/react";
import { useFormContext } from "react-hook-form";
import { useTranslation } from "react-i18next";

const FIELD = "agreeWithTerms";

export const AgreeWithTerms = () => {
  const { register, formState } = useFormContext();
  const { t } = useTranslation();

  const fieldErrors = formState.errors[FIELD];
  const isInvalid = Boolean(fieldErrors?.message);

  return (
    <FormControl isInvalid={isInvalid}>
      <Checkbox {...register(FIELD)}>{t("shared:AgreeWithTerms")}</Checkbox>
      {isInvalid && (
        <FormErrorMessage>{fieldErrors?.message?.toString()}</FormErrorMessage>
      )}
    </FormControl>
  );
};
