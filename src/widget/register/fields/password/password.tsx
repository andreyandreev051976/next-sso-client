import {
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
} from "@chakra-ui/react";
import { useFormContext } from "react-hook-form";
import { useTranslation } from "react-i18next";

const FIELD = "password";

export const Password = () => {
  const { register, formState } = useFormContext();
  const { t } = useTranslation();

  const fieldErrors = formState.errors[FIELD];
  const isInvalid = Boolean(fieldErrors?.message);

  return (
    <FormControl isInvalid={isInvalid}>
      <FormLabel>{t("shared:Password")}</FormLabel>
      <Input {...register("password")} />
      {isInvalid && <FormErrorMessage>{ fieldErrors?.message?.toString() }</FormErrorMessage>}
    </FormControl>
  );
};
