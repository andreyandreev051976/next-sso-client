import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { schema } from "./register.schema";
import { useCallback } from "react";
import { createResourceProvider } from "../../shared/utils/resource/providers";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../shared/routes/routes.config";
import { register } from "./register.resource";
import { useToast } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";

export const registerCore = createResourceProvider(register);

export const useRegistration = () => {
  const form = useForm({
    resolver: yupResolver(schema),
  });
  const { request } = registerCore.useResource();
  const { t } = useTranslation();
  const navigate = useNavigate();
  const toast = useToast();

  const onSubmit: Parameters<typeof form.handleSubmit>[0] = useCallback(
    (data) => {
      request({ data }).then(() => {
        navigate(ROUTES.LOGIN);
        toast({
          status: 'success',
          title: t("shared:CreatedSuccessfully"),
        });
      });
    },
    [request, navigate, toast, t]
  );

  return {
    form,
    onSubmit,
  };
};
