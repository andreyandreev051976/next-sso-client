import { INavigateToRegister } from "./register.type";
import { InnerLink } from "../../../../shared/components/innerLink";
import { ROUTES } from "../../../../shared/routes/routes.config";
import { withNamespace } from "../../../../shared/utils/withNamespace";
import { NAMESPACES } from "../../../../setup/i18n";
import { useTranslation } from "react-i18next";

const NavigateToRegistrationBase = ({
  children,
  ...props
}: INavigateToRegister) => {
  const { t } = useTranslation();
  return (
    <InnerLink to={ROUTES.REGISTER} {...props}>
      {children || t("register:Registration")}
    </InnerLink>
  );
};

export const NavigateToRegistration = withNamespace([NAMESPACES.REGISTER])(
  NavigateToRegistrationBase
);
