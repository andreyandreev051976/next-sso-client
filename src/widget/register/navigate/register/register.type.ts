import { IInnerLink } from "../../../../shared/components/innerLink/innerLink.type";

export interface INavigateToRegister extends Omit<IInnerLink, 'to'> {}