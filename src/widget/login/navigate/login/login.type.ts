import { IInnerLink } from "../../../../shared/components/innerLink/innerLink.type";

export interface INavigateToLogin extends Omit<IInnerLink, 'to'> {}