import { InnerLink } from "../../../../shared/components/innerLink";
import { ROUTES } from "../../../../shared/routes/routes.config";
import { withNamespace } from "../../../../shared/utils/withNamespace";
import { NAMESPACES } from "../../../../setup/i18n";
import { useTranslation } from "react-i18next";
import { INavigateToLogin } from "./login.type";

const NavigateToLoginBase = ({ children, ...props }: INavigateToLogin) => {
  const { t } = useTranslation();
  return (
    <InnerLink to={ROUTES.LOGIN} {...props}>
      {children || t("login:Login")}
    </InnerLink>
  );
};

export const NavigateToLogin = withNamespace([NAMESPACES.LOGIN])(
  NavigateToLoginBase
);
