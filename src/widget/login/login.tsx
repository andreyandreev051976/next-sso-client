import { useTranslation } from "react-i18next";
import { Frame } from "../../shared/components/frame";
import { loginCore } from "./login.logic";
import { Form } from "./form";
import { Username } from "./fields/username";
import { Password } from "./fields/password";
import { Submit } from "./fields/submit";
import { NavigateToRegistration } from "../register/navigate/register";
import { VStack } from "@chakra-ui/react";

export const LoginForm = () => {
  const { t } = useTranslation();
  return (
    <loginCore.Provider>
      <Frame w="100%" maxW="440px" h="600px" display="flex" flexDir="column">
        <Frame.Title>{t("login:Login")}</Frame.Title>
        <Form display="flex" flexDir="column" h="full">
          <Form.Field>
            <Username />
          </Form.Field>
          <Form.Field>
            <Password />
          </Form.Field>
          <VStack mt="auto">
            <NavigateToRegistration />
            <Submit w="full" />
          </VStack>
        </Form>
      </Frame>
    </loginCore.Provider>
  );
};
