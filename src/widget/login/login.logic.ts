import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { schema } from "./login.schema";
import { useCallback } from "react";
import { createResourceProvider } from "../../shared/utils/resource/providers";
import { login } from "./login.resource";
import { useSearchParams } from "react-router-dom";
import { REDIRECT_KEY } from "./login.constants";
import { useToast } from "@chakra-ui/react";
import { AxiosError } from "axios";

export const loginCore = createResourceProvider(login);

export const useLogin = () => {
  const form = useForm({
    resolver: yupResolver(schema),
  });
  const [searchParams] = useSearchParams();
  const { request } = loginCore.useResource();
  const toast = useToast();

  const onSubmit: Parameters<typeof form.handleSubmit>[0] = useCallback(
    (data) => {
      const retpath = searchParams.get(REDIRECT_KEY);
      request({ data, params: { retpath } })
        .then((response) => {
          const redirectPath = retpath || response.data 
          if (redirectPath) {
            window.location.replace(redirectPath);
          }
        })
        .catch((error: AxiosError<{ message: string }>) => {
          const message = error.response?.data?.message;
          toast({
            status: "error",
            title: "Ошибка",
            ...message && { description: message }
          });
        });
    },
    [request, toast, searchParams]
  );

  return {
    form,
    onSubmit,
  };
};
