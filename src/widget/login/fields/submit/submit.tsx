import { Button } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";
import { loginCore } from "../../login.logic";
import { ISubmit } from "./submit.type";

export const Submit = ({ type = "submit", children, ...props }: ISubmit) => {
  const { t } = useTranslation();
  const { state } = loginCore.useResource();

  return (
    <Button {...props} isLoading={state.isLoading} type={type}>
      {children || t("shared:Continue")}
    </Button>
  );
};
