import { Box, chakra } from "@chakra-ui/react";
import { IField } from "./field.type";

const FieldBox = chakra(Box, {
  baseStyle: {
    _notLast: {
      mb: 4,
    },
  },
});

export const Field = (props: IField) => <FieldBox {...props} />;
