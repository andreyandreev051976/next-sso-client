import { FormProvider } from "react-hook-form";
import { useLogin } from "../login.logic";
import { IForm } from "./form.type";
import { Field } from "./components/field";
import { Box } from "@chakra-ui/react";

export const Form = ({ children, ...props }: IForm) => {
  const { form, onSubmit } = useLogin();

  return (
    <FormProvider {...form}>
      <Box as="form" {...props} onSubmit={form.handleSubmit(onSubmit)}>
        {children}
      </Box>
    </FormProvider>
  );
};

Form.Field = Field;
