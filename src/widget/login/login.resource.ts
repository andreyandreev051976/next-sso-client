import { resource } from "../../shared/utils/resource";

interface ILoginDTO {
  password: string;
  email: string;
}

const API_URL = import.meta.env.VITE_API_URL;

export const login = resource<ILoginDTO, string>({
  url: `${API_URL}/auth/login`,
  method: "POST",
  withCredentials: true
});
