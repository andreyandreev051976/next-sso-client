import {
  Navigate,
  RouterProvider,
  createBrowserRouter,
} from "react-router-dom";
import "./App.css";
import { ROUTES } from "./shared/routes/routes.config";
import { BaseLayout } from "./shared/layout";
import { LoginForm } from "./widget/login";
import { NamespaceLoader } from "./shared/components/i18n/namespaceLoader";
import { NAMESPACES } from "./setup/i18n";
import { RegisterForm } from "./widget/register";

const router = createBrowserRouter([
  {
    path: ROUTES.BASE,
    element: (
      <BaseLayout>
        <BaseLayout.Content />
      </BaseLayout>
    ),
    children: [
      {
        path: ROUTES.LOGIN,
        element: (
          <BaseLayout.Container centerContent justifyContent="center">
            <NamespaceLoader ns={NAMESPACES.LOGIN}>
              <LoginForm />
            </NamespaceLoader>
          </BaseLayout.Container>
        ),
      },
      {
        path: ROUTES.REGISTER,
        element: (
          <BaseLayout.Container centerContent justifyContent="center">
            <NamespaceLoader ns={NAMESPACES.REGISTER}>
              <RegisterForm />
            </NamespaceLoader>
          </BaseLayout.Container>
        ),
      },
      {
        path: "*",
        element: <Navigate to={ROUTES.LOGIN} replace />,
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
