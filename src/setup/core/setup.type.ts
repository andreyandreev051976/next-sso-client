export interface ISetupFn {
    (): void
}

export interface IRunSetup {
    (fns: ISetupFn[]): void
}