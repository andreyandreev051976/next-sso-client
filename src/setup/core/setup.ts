import { IRunSetup, ISetupFn } from "./setup.type";

export const createSetupFn = <Fn extends ISetupFn>(fn: Fn) => fn; 

export const runSetup: IRunSetup = (fns = []) => fns.forEach(fn => fn());