import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import { createSetupFn } from "../core";

import Backend from "i18next-http-backend";
import LanguageDetector from "i18next-browser-languagedetector";

export const NAMESPACES = {
  LOGIN: "login",
  REGISTER: "register",
  SHARED: "shared",
};

export const setupI18n = createSetupFn(() => {
  i18n
    .use(Backend)
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
      ns: [NAMESPACES.LOGIN, NAMESPACES.REGISTER, NAMESPACES.SHARED],
      defaultNS: NAMESPACES.SHARED,
      fallbackLng: "ru",
      interpolation: {
        escapeValue: false,
      },
    });

  i18n.loadNamespaces(NAMESPACES.SHARED)
});
