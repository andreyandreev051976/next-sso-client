export interface AnyFn {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (...args: any): any
}

export type ObjValues<T> = T[keyof T]