import { NetworkError } from "./abstract-error";

export class BadRequest extends NetworkError implements NetworkError {
    status: number = 400;
}