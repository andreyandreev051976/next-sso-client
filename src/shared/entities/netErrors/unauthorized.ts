import { NetworkError } from "./abstract-error";

export class Unauthorized extends NetworkError {
    status: number = 401;
}