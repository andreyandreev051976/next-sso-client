import { NetworkError } from "./abstract-error";
import { BadRequest } from "./bad-request";
import { Forbidden } from "./forbidden";
import { Unauthorized } from "./unauthorized";

const ERROR_INSTANCES = [BadRequest, Forbidden, Unauthorized];

export const ERROR_INSTANCE_BY_STATUS = ERROR_INSTANCES.reduce((map, error) => {
    map[error.status] = error;

    return map
}, {} as Record<number, typeof NetworkError>)