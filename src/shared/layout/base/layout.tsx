import { Container, Box } from "@chakra-ui/react";
import { Outlet } from "react-router-dom";
import { IContainer, ILayout } from "./layout.type";

export const BaseContainer = (props: IContainer) => {
  return (
    <Container maxW="1280px" h="full" {...props} />
  );
};

export const BaseLayout = (props: ILayout) => {
  return (
    <Box h="full" bg="var(--chakra-colors-gradients-coolBlues)" {...props} />
  );
};

BaseLayout.Container = BaseContainer;
BaseLayout.Content = Outlet;
