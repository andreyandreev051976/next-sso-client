import { BoxProps, ContainerProps } from "@chakra-ui/react";

export interface ILayout extends BoxProps {}
export interface IContainer extends ContainerProps {}
