import { TextProps } from "@chakra-ui/react";

export interface ITitle extends TextProps {}