import { PropsWithChildren, ReactNode } from "react"
import { NAMESPACES } from "../../../../setup/i18n"
import { ObjValues } from "../../../types/alias"

export interface INamespaceLoader extends PropsWithChildren {
    ns: ObjValues<typeof NAMESPACES> | Array<ObjValues<typeof NAMESPACES>>,
    fallback?: ReactNode
}