import { IInnerLink } from "./innerLink.type";
import { Link } from '@chakra-ui/react';
import { Link as RouterLink } from 'react-router-dom';

export const InnerLink = ({ ...props }: IInnerLink) => {
    return <Link {...props} as={RouterLink} />
}