import { LinkProps } from "@chakra-ui/react";
import { ObjValues } from "../../types/alias";
import { ROUTES } from "../../routes/routes.config";

export interface IInnerLink extends Omit<LinkProps, 'as'> {
    to: ObjValues<typeof ROUTES>,
}