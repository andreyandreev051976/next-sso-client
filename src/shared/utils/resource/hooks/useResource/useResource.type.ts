import { IResourceCaller } from "../../resource.type";

export interface IPendingResponse {
    isSuccess: null,
    isLoading: boolean,
    errors: [],
}

export interface ISuccessResponse {
    isSuccess: true,
    isLoading: boolean,
    errors: [],
}

export interface IInvalidResponse {
    isSuccess: false,
    isLoading: boolean,
    errors: unknown[],
}

export type IState = ISuccessResponse | IInvalidResponse | IPendingResponse;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type IAnyResourceCaller = IResourceCaller<any, any>

export interface IResourceRequest<R extends IAnyResourceCaller> {
    request: R;
}

export interface IUseResourceState {
    state: IState, 
}

export type IUseResourceReturn<R extends IAnyResourceCaller> = (IUseResourceState & IResourceRequest<R>);

export interface IUseResource {
    <R extends IAnyResourceCaller>(resource: R): IUseResourceReturn<R>
}