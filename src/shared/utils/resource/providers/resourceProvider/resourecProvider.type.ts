import { Consumer, Context, PropsWithChildren, ReactNode } from "react";
import { IResourceCaller } from "../../resource.type";
import { IUseResourceReturn } from "../../hooks/useResource/useResource.type";

export interface IResourceProvider {
    (props: PropsWithChildren): ReactNode
}

export interface IUseResource<R> {
    (): R
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type IAnyResourceCaller = IResourceCaller<any, any>;

export interface IResourceProviderInstance<R extends IAnyResourceCaller> {
    Provider: IResourceProvider,
    Context: Context<IUseResourceReturn<R>>,
    Consumer: Consumer<IUseResourceReturn<R>>,
    useResource: IUseResource<IUseResourceReturn<R>>
}

export interface ICreateResourceProvider {
    <R extends IAnyResourceCaller>(resource: R): IResourceProviderInstance<R>
}