import { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";
import { IResourcePlugin } from "./plugins/create/create.type";

interface IResourceConfig<D, R, E, C> extends AxiosRequestConfig<D> {
  onSuccess?: <T = AxiosResponse<R, C>>(data: T) => T;
  onError?: <T = AxiosError<E, C>>(error: T) => T;
  onFinally?: () => void;
  plugins?: IResourcePlugin<D, D, E>[];
}

export interface IResourceCaller<D, R, C> {
  (params?: AxiosRequestConfig<D>): Promise<AxiosResponse<R, C>>;
}

export interface IResource {
  <D, R = unknown, E = unknown, C = object>(
    params: IResourceConfig<D, R, E, C>
  ): IResourceCaller<D, R, C>;
}
