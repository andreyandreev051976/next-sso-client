import axios, { AxiosError } from "axios";
import { IResource } from "./resource.type";
import { noop, noopValue } from "../noop";
import { callPlugin } from "./plugins/call";
import { IResourcePlugin } from "./plugins/create/create.type";
import { isRedirect, isSuccess } from "./resource.constants";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type AnyPlugin = IResourcePlugin<any, any, any>;

const pluginsCache = new Map<
  AnyPlugin[],
  Record<AnyPlugin["trigger"], AnyPlugin["handler"][]>
>();

const processPlugins = (plugins: AnyPlugin[]) => {
  const groupedPlugins = plugins.reduce((group, plugin) => {
    (group[plugin.trigger] = group[plugin.trigger] || []).push(plugin.handler);

    return group;
  }, {} as Record<AnyPlugin["trigger"], AnyPlugin["handler"][]>);

  pluginsCache.set(plugins, groupedPlugins);

  return groupedPlugins;
};

export const resource: IResource = ({
  onSuccess = noopValue,
  onError = noopValue,
  onFinally = noop,
  plugins = [],
  ...config
}) => {
  const computedPlugins = pluginsCache.get(plugins) || processPlugins(plugins);
  callPlugin(computedPlugins.request)(config);

  return (externalConfig = {}) =>
    axios({ ...config, ...externalConfig })
      .then((d) => {
        if(!isSuccess(d.status) && !isRedirect(d.status)){
          throw new AxiosError(d.data);
        }
        callPlugin(computedPlugins.success)(d);
        return onSuccess(d);
      })
      .catch((e) => {
        callPlugin(computedPlugins.error)(e);
        onError(e);
        throw e;
      })
      .finally(() => {
        callPlugin(computedPlugins.finally)();
        onFinally();
      });
};
