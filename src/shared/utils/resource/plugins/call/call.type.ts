import { AnyFn } from "../../../../types/alias";

export interface ICallPlugin {
    <Fn extends AnyFn>(fns: Fn[]): (...args: Parameters<Fn>) => void 
}