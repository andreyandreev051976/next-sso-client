import { AxiosError } from "axios";
import { IResourceErrorPlugin } from "../create/create.type";

export interface IOnErrorCallback {
    <T, D>(error: AxiosError<T, D>): void;
}

export interface IOnError extends Record<number, IOnErrorCallback> {}

export interface IAutoErrorOptions {
    onError?: IOnError
}

export interface IAutoError {
    (options?: IAutoErrorOptions): IResourceErrorPlugin<AxiosError<unknown, unknown>>,
}