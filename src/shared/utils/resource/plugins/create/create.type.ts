export interface IResourceRequestPlugin<D> {
  trigger: "request";
  handler: (data: D) => void;
}

export interface IResourceResponsePlugin<D> {
  trigger: "response";
  handler: (data: D) => void;
}

export interface IResourceSuccessPlugin<D> {
  trigger: "success";
  handler: (data: D) => void;
}

export interface IResourceErrorPlugin<D> {
  trigger: "error";
  handler: (data: D) => void;
}

export interface IResourceFinallyPlugin {
  trigger: "finally";
  handler: () => void;
}

export type IResourcePlugin<D, R, E> =
  | IResourceRequestPlugin<D>
  | IResourceResponsePlugin<R>
  | IResourceSuccessPlugin<R>
  | IResourceErrorPlugin<E>
  | IResourceFinallyPlugin;
