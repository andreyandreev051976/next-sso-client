import {
  IResourceRequestPlugin,
  IResourceResponsePlugin,
  IResourceSuccessPlugin,
  IResourceErrorPlugin,
  IResourceFinallyPlugin,
} from "./create.type";

export const createRequestPlugin = <D>(
  handler: IResourceRequestPlugin<D>["handler"]
): IResourceRequestPlugin<D> => ({
  handler,
  trigger: "request",
});

export const createResponsePlugin = <D>(
  handler: IResourceResponsePlugin<D>["handler"]
): IResourceResponsePlugin<D> => ({
  handler,
  trigger: "response",
});

export const createSuccessPlugin = <D>(
  handler: IResourceSuccessPlugin<D>["handler"]
): IResourceSuccessPlugin<D> => ({
  handler,
  trigger: "success",
});

export const createFinallyPlugin = (
  handler: IResourceFinallyPlugin["handler"]
): IResourceFinallyPlugin => ({
  handler,
  trigger: "finally",
});

export const createErrorPlugin = <D>(
  handler: IResourceErrorPlugin<D>["handler"]
): IResourceErrorPlugin<D> => ({
  handler,
  trigger: "error",
});
