import { INoopValue } from "./noop.type";

export const noop = () => {};
export const noopValue: INoopValue = value => value;