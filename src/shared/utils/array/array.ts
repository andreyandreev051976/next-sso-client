import { IToArray } from "./array.type";

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
export const toArray: IToArray = (target) => Array.isArray(target) ? target : [target];
