import { ComponentType, ReactNode } from "react";

export interface IWithNamespace {
    (namespace: string | string[]): <T>(Component: ComponentType<T>) => (props: T) => ReactNode;
}