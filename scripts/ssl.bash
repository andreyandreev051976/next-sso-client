#!/usr/bin/env bash

host_name='local-sso.my-awesome-domain.ru'

rm -rf .cert
mkdir -p .cert
mkcert -key-file .cert/key.pem -cert-file .cert/cert.pem "$host_name"
chmod -R 777 .cert

host_proxy="127.0.0.1 $host_name"
host_path=/etc/hosts

grep -qxF "$host_proxy" $host_path || echo $host_proxy >> $host_path